#include <iostream>
#include <csignal>

static bool not_stopping = true;
void end_signal_handler(int sig){
  not_stopping = false;
}

int main() {

  std::signal(SIGINT, &end_signal_handler);
  while(not_stopping){
    std::cout << ".";
  }

  std::cout << "\n\n"
            << "Done."
            << std::endl;

  return 0;
}

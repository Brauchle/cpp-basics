//============================================================================
// Name        : TemplatePerformance.cpp
// Author      : Florian Brauchle
// Version     :
// Copyright   : -
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

#include "test_function.h"
#include "test_function_inline.h"
#include "test_function_template.h"

#include <chrono>
//#include <ctime> apparently outdated and not recommended / less acurate /less save

int main() {
    int limit = 100000000;

    cout << limit << " Calculations" << endl;

    chrono::high_resolution_clock::time_point t1;
    chrono::high_resolution_clock::time_point t2;

    chrono::duration<double> ms_time;

    int fakoff = 12;




    // CONST EXPRESSION TEST
    cout << "Const Expression Test: " << endl;
    t1 = chrono::high_resolution_clock::now();
	for(int i = 0; i < limit; i++){
		int res1 = fak(fakoff);
	}
    t2 = chrono::high_resolution_clock::now();

    ms_time = chrono::duration_cast<chrono::milliseconds>(t2-t1);
	cout << "Fak  : " << ms_time.count() << endl;


    t1 = chrono::high_resolution_clock::now();
	for(int i = 0; i < limit; i++){
		constexpr int res2 = fak_c(12);
	}
    t2 = chrono::high_resolution_clock::now();

    ms_time = chrono::duration_cast<chrono::milliseconds>(t2-t1);
	cout << "Fak c: " << ms_time.count() << endl;



    // Template & Inline TEST
    cout << endl << "Template & Function Test: " << endl;
    t1 = chrono::high_resolution_clock::now();
	for(int i = 0; i < limit; i++){
		fak(fakoff);
	}
    t2 = chrono::high_resolution_clock::now();

    ms_time = chrono::duration_cast<chrono::milliseconds>(t2-t1);
	cout << "Fak  : " <<  ms_time.count() << endl;


    t1 = chrono::high_resolution_clock::now();
	for(int i = 0; i < limit; i++){
		fak_i(fakoff);
	}
    t2 = chrono::high_resolution_clock::now();

    ms_time = chrono::duration_cast<chrono::milliseconds>(t2-t1);
	cout << "Fak i: " <<  ms_time.count() << endl;


    t1 = chrono::high_resolution_clock::now();
	for(int i = 0; i < limit; i++){
		fak_t(fakoff);
	}

    t2 = chrono::high_resolution_clock::now();

    ms_time = chrono::duration_cast<chrono::milliseconds>(t2-t1);
	cout << "Fak t: " << ms_time.count() << endl;


    cout << endl << "Done" << endl;

	return 0;
}

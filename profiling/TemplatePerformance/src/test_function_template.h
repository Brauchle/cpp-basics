/*
 * test_function_template.h
 *
 *  Created on: Oct 6, 2018
 *      Author: flo
 */

#ifndef TEST_FUNCTION_TEMPLATE_H_
#define TEST_FUNCTION_TEMPLATE_H_

template <typename T>
T fak_t (T a) {
	if(a>1){
		return a * fak_t(a-1);
	} else {
		return 1;
	}
}


template <typename T>
void n_fak_t (int a, T b) {
	for(int i = 0; i<a; i++){
		fak_t(i);
	}
}


template <typename T>
T test_add_t (T a, T b) {
	return a + b;
}

template <typename T>
T test_sum_t (T a) {
	T res = 0;
	for(int i = 0; i < a; i++){
		res = test_add_t(res, T(i));
	}
	return res;
}


#endif /* TEST_FUNCTION_TEMPLATE_H_ */

/*
 * test_function.h
 *
 *  Created on: Oct 6, 2018
 *      Author: flo
 */

#ifndef TEST_FUNCTION_H_
#define TEST_FUNCTION_H_


int fak(int);
constexpr int fak_c(int);
void n_fak(int, int);

int test_add(int, int);
int test_sum(int);


constexpr int fak_c(int a){
	if(a>1){
		return a * fak_c(a-1);
	} else {
		return 1;
	}
}



#endif /* TEST_FUNCTION_H_ */

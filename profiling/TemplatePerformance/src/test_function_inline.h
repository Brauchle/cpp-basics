/*
 * test_function_inline.h
 *
 *  Created on: Oct 6, 2018
 *      Author: flo
 */

#ifndef TEST_FUNCTION_INLINE_H_
#define TEST_FUNCTION_INLINE_H_



int fak_i(int a){
	if(a>1){
		return a * fak_i(a-1);
	} else {
		return 1;
	}
};

void n_fak_i(int a, int b){
	for(int i = 0; i<a; i++){
		fak_i(i);
	}
};

int test_add_il(int a, int b){
	return a + b;
}

int test_sum_i(int a){
	int res = 0;
	for(int i = 0; i < a; i++){
		res = test_add_il(res, i);
	}
	return res;
}

#endif /* TEST_FUNCTION_INLINE_H_ */

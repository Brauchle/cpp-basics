/*
 * test_function.cpp
 *
 *  Created on: Oct 6, 2018
 *      Author: flo
 */

#include "test_function.h"


int fak(int a){
	if(a>1){
		return a * fak(a-1);
	} else {
		return 1;
	}
}


void n_fak(int a, int b){
	for(int i = 0; i<a; i++){
		fak(i);
	}
};


int test_add(int a, int b){
	return a+b;
}


int test_sum(int a){
	int res = 0;
	for(int i = 0; i < a; i++){
		res = test_add(i, res);
	}
	return res;
}


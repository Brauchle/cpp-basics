#include <iostream>
#include <chrono>
#include <cstdlib>
#include <vector>
#include <complex>


void multiplyAdd_gl(
    std::vector<std::complex<float>>& buffer1,
    std::vector<std::complex<float>>& buffer2,
    float factor) {

  buffer1[0] = buffer2[0] * factor;
  for (int i = 1; i < buffer1.size()-1; ++i){
    buffer1[i] = buffer2[i] * factor;
    buffer2[i] = buffer2[i] * factor;
  }
  buffer2[buffer1.size()] = buffer2[buffer1.size()] * factor;
}

void multiplyAdd_bl(
    std::vector<std::complex<float>>& buffer1,
    std::vector<std::complex<float>>& buffer2,
    float factor) {
  for (int i = 0; i < buffer1.size()-1; ++i){
    buffer1[i]   = buffer2[i] * factor;
    buffer2[i+1] = buffer2[i+1] * factor;
  }
}

int main(int argc, char const *argv[]) {
  int v_size = 10280;
  const int repetitions = 100000;

  std::chrono::high_resolution_clock::time_point t1;
  std::chrono::high_resolution_clock::time_point t2;

  std::chrono::duration<double> ms_time;

  std::cout << " Good Loop" << std::endl;
  std::vector<std::complex<float>> b1;
  std::vector<std::complex<float>> b2;

  b1.resize(v_size);
  b2.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = i/v_size;
    b2[i] = i/v_size;
  }

  t1 = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < repetitions; i++) {
    multiplyAdd_gl(b1, b2, 0.001f);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << std::endl;



  std::cout << " Bad Loop" << std::endl;
  b1.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = i/v_size;
    b2[i] = i/v_size;
  }

  t1 = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < repetitions; i++) {
    multiplyAdd_bl(b1, b2, 0.001f);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
  std::cout << "time  : " << ms_time.count() << std::endl;

  return 0;
}

#include <iostream>
#include <chrono>
#include <cstdlib>
#include <vector>

#include <random>
// clang++ cache_omp_f -Ofast -fopenmp

float average_abs(const std::vector<float>& buffer1) {
  float res = 0;

  for (const float& e : buffer1){
    res += e;
  }
  res = res / buffer1.size();
  return res;
}

int main(int argc, char const *argv[]) {
  int v_size = 1000000;
  const int repetitions = 10000;
  int steps = 2;

  std::random_device rd{};
  std::mt19937 gen{rd()};
  std::normal_distribution<> d{0,2};

  std::chrono::high_resolution_clock::time_point t1;
  std::chrono::high_resolution_clock::time_point t2;
  std::chrono::duration<double> ms_time;

  std::vector<float> b1;
  float res_abs;

  v_size = v_size/sizeof(float);
  std::cout << v_size/1000000.0 * sizeof(float) << " MB Data" << std::endl;
  b1.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = d(gen);
  }

  t1 = std::chrono::high_resolution_clock::now();
  #pragma omp parallel for
  for (size_t i = 0; i < repetitions; i++) {
    res_abs = average_abs(b1);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << " " << res_abs << std::endl;



  v_size = v_size * steps;
  std::cout << v_size/1000000.0 * sizeof(float) << " MB Data" << std::endl;
  b1.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = d(gen);
  }

  t1 = std::chrono::high_resolution_clock::now();
  #pragma omp parallel for
  for (size_t i = 0; i < repetitions; i++) {
    res_abs = average_abs(b1);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << " " << res_abs << std::endl;




  v_size = v_size * steps;
  std::cout << v_size/1000000.0 * sizeof(float) << " MB Data" << std::endl;
  b1.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = d(gen);
  }

  t1 = std::chrono::high_resolution_clock::now();
  #pragma omp parallel for
  for (size_t i = 0; i < repetitions; i++) {
    res_abs = average_abs(b1);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << " " << res_abs << std::endl;




  v_size = v_size * steps;
  std::cout << v_size/1000000.0 * sizeof(float) << " MB Data" << std::endl;
  b1.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = d(gen);
  }

  t1 = std::chrono::high_resolution_clock::now();
  #pragma omp parallel for
  for (size_t i = 0; i < repetitions; i++) {
    res_abs = average_abs(b1);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << " " << res_abs << std::endl;




  v_size = v_size * steps;
  std::cout << v_size/1000000.0 * sizeof(float) << " MB Data" << std::endl;
  b1.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = d(gen);
  }

  t1 = std::chrono::high_resolution_clock::now();
  #pragma omp parallel for
  for (size_t i = 0; i < repetitions; i++) {
    res_abs = average_abs(b1);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << " " << res_abs << std::endl;




  v_size = v_size * steps;
  std::cout << v_size/1000000.0 * sizeof(float) << " MB Data" << std::endl;
  b1.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = d(gen);
  }

  t1 = std::chrono::high_resolution_clock::now();
  #pragma omp parallel for
  for (size_t i = 0; i < repetitions; i++) {
    res_abs = average_abs(b1);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << " " << res_abs << std::endl;




  v_size = v_size * steps;
  std::cout << v_size/1000000.0 * sizeof(float) << " MB Data" << std::endl;
  b1.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = d(gen);
  }

  t1 = std::chrono::high_resolution_clock::now();
  #pragma omp parallel for
  for (size_t i = 0; i < repetitions; i++) {
    res_abs = average_abs(b1);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << " " << res_abs << std::endl;




  v_size = v_size * steps;
  std::cout << v_size/1000000.0 * sizeof(float) << " MB Data" << std::endl;
  b1.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = d(gen);
  }

  t1 = std::chrono::high_resolution_clock::now();
  #pragma omp parallel for
  for (size_t i = 0; i < repetitions; i++) {
    res_abs = average_abs(b1);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << " " << res_abs << std::endl;


  return 0;
}

#include <iostream>
#include <algorithm>
#include <random>


bool is_prime(int number){
  // std::cout << number << std::endl << std::endl;
  for(int i = 2; i < number; ++i){
    // std::cout << number%i << std::endl;
    if( (number%i) == 0 ){
      return false;
    }
  }
  return true;
}


int main(){
  std::cout << "Input : ";
  int number;
  std::cin >> number;

  int p_cnt = 0;
  for(int j=0; j<1000000; j++){
    if( is_prime(number+j) ) {
      p_cnt++;
    }
  }
  std::cout << p_cnt << " Primes." << std::endl;

  return 1;
}

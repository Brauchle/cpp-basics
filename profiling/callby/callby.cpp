#include <iostream>
#include <chrono>
#include <cstdlib>
#include <vector>
#include <complex>


std::vector<std::complex<float>> multiplyAdd_cbv(std::vector<std::complex<float>> buffer1, float factor){
  for (std::complex<float>& e : buffer1) {
    e += e * factor;
  }
  return buffer1;
}

void multiplyAdd_cbr(std::vector<std::complex<float>>& buffer1, float factor){
  for (std::complex<float>& e : buffer1) {
    e += e * factor;
  }
}

int main(int argc, char const *argv[]) {
  int v_size = 10280;
  const int repetitions = 100000;

  std::chrono::high_resolution_clock::time_point t1;
  std::chrono::high_resolution_clock::time_point t2;

  std::chrono::duration<double> ms_time;

  std::cout << v_size << " Call by Value ()" << std::endl;
  std::vector<std::complex<float>> b1;

  b1.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = i/v_size;
  }

  t1 = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < repetitions; i++) {
    b1 = multiplyAdd_cbv(b1, 0.001f);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << std::endl;



  std::cout << v_size << " Call by Reference" << std::endl;
  b1.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = i/v_size;
  }

  t1 = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < repetitions; i++) {
    multiplyAdd_cbr(b1, 0.001f);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
  std::cout << "time  : " << ms_time.count() << std::endl;

  return 0;
}


import threading

import time
import tkinter as tk

import numpy as np

import matplotlib
matplotlib.use("TkAgg")
from matplotlib import pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
from matplotlib.animation import FuncAnimation

# Parameter
reset_time = 2
shutdown_now = False
update_int = 0.2


# setup tinker
root = tk.Tk()
root.title('GUI Demonstrator')

# Matplotlib GUI Stuff
fig  = plt.figure(figsize=(5, 4), dpi=100)
ax   = plt.axes(xlim=(0, 4), ylim=(-2, 2))
line, = ax.plot([], [], lw=3)

canvas = FigureCanvasTkAgg(fig, master=root)  # A tk.DrawingArea.
canvas.draw()
canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

toolbar = NavigationToolbar2TkAgg(canvas, root)
toolbar.update()
canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

# Matplotlib animation
def init_ani():
    line.set_data([], [])
    return line,
def update_ani(i):
    x = np.linspace(0, 4, 1000)
    # y = np.sin(2 * np.pi * (x - 0.01 * i))
    y = np.random.randn(1, 1000)
    line.set_data(x, y)
    return line,

anim = FuncAnimation(fig, update_ani, interval=50) # interval in ms
# End Threads on Close Window
def end_it():
    global shutdown_now
    shutdown_now = True
    plt.close()
    time.sleep(2*update_int)
    root.destroy()

root.protocol("WM_DELETE_WINDOW", end_it)

# Background Thread for LoRa Receiver
class Backend(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        print("# Starting Thread for LoRa Receiver")

    def run(self):
        while not shutdown_now:
            # sleep to make room for others
            time.sleep(0.01)
        # Thread Ended
        print(' . thread ended ')



# Start Program
w = Backend()
w.start()

root.mainloop()

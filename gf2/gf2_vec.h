#ifndef GF2_VEC_H
#define GF2_VEC_H

#include <vector>
#include <iostream>

using bitvec = std::vector<int>;

bitvec gf2add(const bitvec& a, const bitvec& b);
void   gf2addt(bitvec& a, const bitvec& b);
bitvec gf2mult(bitvec& a, bitvec& b);
bool   gf2div(const bitvec& product,  bitvec& a, bitvec& b);

int    gf2grad(const bitvec& in);
bitvec gf2shift(const bitvec& in, int i);

std::ostream& operator<< (std::ostream &out, const bitvec &pos);



#endif // DATA_GEN_H

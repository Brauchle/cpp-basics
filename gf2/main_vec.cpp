#include <iostream>

#include "gf2_vec.h"


int main() {
  bitvec a{0, 1, 0, 0, 0};
  bitvec b;
  b.assign(5, 0);
  bitvec c{0, 0, 0, 1, 1, 0, 1};

  std::cout << "TEST" << std::endl;
  std::cout << gf2grad(a) << ": " << a << std::endl;
  std::cout << gf2grad(b) << ": " << b << std::endl;
  std::cout << gf2grad(c) << ": " << c << std::endl;

  bitvec mult = gf2mult(a, c);
  std::cout << "Mult: " << mult << std::endl;

  bitvec add = gf2add(a, c);
  std::cout << "Add : " << add << std::endl;

  add = gf2add(a, a);
  std::cout << "Add : " << add << std::endl;

  bool q = gf2div(c, a, b);
  std::cout << "worked ? " << q << std::endl;
  std::cout << "a   : " << c << std::endl;
  std::cout << "b   : " << a << std::endl;
  std::cout << "q   : " << b << std::endl;
  std::cout << "end main " << std::endl;

  return 0;
}

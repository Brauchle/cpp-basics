#include "gf2_vec.h"

bitvec gf2add(const bitvec& a,  const bitvec& b){
  bitvec tmp_sum = a;
  int len = a.size();

  if( a.size() > b.size() ){
    len = b.size();
    tmp_sum = a;
  } else {
    len = a.size();
    tmp_sum = b;
  }

  for(int i = 0; i < len; ++i){
    tmp_sum[i] = (a[i] + b[i])%2;
  }

  return tmp_sum;
}

bitvec gf2mult(bitvec& a, bitvec& b){
  bitvec result;
  int grad_a = gf2grad(a);
  int grad_b = gf2grad(b);
  result.assign(grad_a + grad_b + 1, 0);

  for(int i = 0; i <= grad_b; ++i){
    for(int j = 0; j <= grad_a; ++j){
      if( b[i] == 1) {
        result[j+i] = (result[j+i] + a[j])%2;
      }
    }
  }

  return result;
}

int gf2grad(const bitvec& in){
  if(in.empty()){
    return -1;
  }

  for (int i = in.size()-1; i > -1; --i) {
    if( in[i] == 1 ){
      return i;
    }
  }
  return -1;
}

bitvec gf2shift(const bitvec& in, int i){
  int n_size = gf2grad(in) + 1;
  bitvec res;
  res.resize(n_size + i, 0);

  for (int j = 0; j < n_size; ++j) {
    res[j + i] = in[j];
  }

  return res;
}

bool gf2div(const bitvec& product, bitvec& a, bitvec& b){
  bitvec in = product;
  int div = gf2grad(in) - gf2grad(a);
  if( div < 0 ){
    return false;
  }

  b.assign(div+1, 0);

  bitvec mres;
  while( div > -1 ){
    b[div] = 1;

    mres = gf2shift(a, div);
    for (size_t i = 0; i < mres.size(); i++) {
      in[i] = (in[i] + mres[i])%2;
    }

    div = gf2grad(in) - gf2grad(a);
  }

  if( gf2grad(in) == -1) {
    return true;
  }

  return false;
}

std::ostream& operator<< (std::ostream &out, const bitvec& in){
  for(int i = in.size(); i > 0; --i){
    out << in[i-1] << " ";
  }
  return out;
}

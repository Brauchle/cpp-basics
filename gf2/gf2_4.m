
% GF 2^4 Field
p=2;
m=4;
[field, expformat] = gftuple([-1:p^m-2]',m,p);

% x_gf enthält alle Werte des GF(2^4)
x = 0:15;
x_gf = -1:14;
y_gf = -1:14;

% Polynom a x^2 + b x + c
% a, b und c sind elemente des Felds (0:15)
a = 0;
b = 13;
c = -1;

for i=1:16
    y_1 = gfmul(x_gf(i), x_gf(i), field);
    y_1 = gfmul(a, y_1, field);
    
    y_2 = gfmul( x_gf(i), b, field); 
    
    y_gf(i) = gfadd(y_1, y_2, field);
    y_gf(i) = gfadd(y_gf(i), c, field);
end
 
y = y_gf + 1;
y( y < 1 ) = 0;

stem(x, y);
#ifndef SPECTOGRAM_H
#define SPECTOGRAM_H

#include <QtDataVisualization/Q3DSurface>
#include <QtDataVisualization/QSurfaceDataProxy>
#include <QtDataVisualization/QHeightMapSurfaceDataProxy>
#include <QtDataVisualization/QSurface3DSeries>
#include <QtWidgets/QSlider>

using namespace QtDataVisualization;

class Spectogram : public QObject
{
    Q_OBJECT
public:
    explicit Spectogram(Q3DSurface *surface);
    ~Spectogram();


public Q_SLOTS:
    void updatePlotData();

private:
    Q3DSurface *m_graph;
    QSurfaceDataProxy *m_CyclicProxy;
    QSurface3DSeries *m_CyclicSeries;

    QSurfaceDataArray *dataArray; // Speichert die Daten

    int m_fft_len;
    int m_alpha_len;

    void update_data(); // Für regelmäßige updates mit gleichen aprameterern
    void update_parameters(); // Alles muss neu berechnet werden und speicher neu belegt
    void init_data();
};


#endif // SPECTOGRAM_H

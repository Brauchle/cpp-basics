#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "QTimer"

#include "data_gen.h"

#include <QtWidgets/QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    initPlotData();
    curve1 = new QwtPlotCurve( "Curve 1" );
    curve2 = new QwtPlotCurve( "Curve 2" );

    //curve1->setSamples(x, y1, 500);
    //curve2->setSamples(x, y2, 500);
    curve1->setRawSamples(x, y, 500);

    //curve1->attach( ui->qwtPlot );

    //ui->qwtPlot->replot();

    // SPectogram

    graph = new Q3DSurface();
    container = QWidget::createWindowContainer(graph);

    if (!graph->hasContext()) {
        QMessageBox msgBox;
        msgBox.setText("Couldn't initialize the OpenGL context.");
        msgBox.exec();
    }

    container->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    container->setFocusPolicy(Qt::StrongFocus);

    cyc_plt = new Spectogram(graph);

    ui->verticalLayout_3->insertWidget(0, container, 1);

    // timer für Plot Updates
    timer = new QTimer(this);
    QObject::connect(timer, SIGNAL(timeout()), cyc_plt, SLOT(updatePlotData()));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete curve1;
    delete curve2;
}

void MainWindow::on_B_startPlot_clicked()
{
    timer->start(500);
}

void MainWindow::initPlotData(){
    for (int i=0;i<500;i++) {
        x[i]  = i;
        y[i] = i;
    }
}

void MainWindow::updatePlotData(){
    //gen_data(x, y, 500);
    //curve1->setSamples(x, y1, 500);
    //curve2->setSamples(x, y2, 500);

    //ui->qwtPlot->setAxisScale(0, -5, 5, 0);
    //ui->qwtPlot->setAxisScale(2, -5, 5, 0);
}

void MainWindow::on_bPause_clicked()
{
    timer->stop();
}

void MainWindow::on_refreshRate_valueChanged(double arg1)
{
    timer->start(std::chrono::milliseconds(int( (1.0/arg1) * 1000.0 )));
}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "spectogram.h"

#include <QMainWindow>
#include <qwt_plot_curve.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_B_startPlot_clicked();
    void updatePlotData();

    void on_bPause_clicked();

    void on_refreshRate_valueChanged(double arg1);

private:
    Ui::MainWindow *ui;

    // plot data
    QTimer *timer;
    // add curves
    QwtPlotCurve *curve1;
    QwtPlotCurve *curve2;
    double x[500];
    double y[500];

    void initPlotData();

    // Surface Plot
    Q3DSurface *graph;
    QWidget *container;

    Spectogram *cyc_plt;
};

#endif // MAINWINDOW_H

#include "spectogram.h"

#include <random>

#include <QtDataVisualization/QValue3DAxis>
#include <QtDataVisualization/Q3DCamera>
#include <QtCore/qmath.h>
#include <QtDataVisualization/Q3DTheme>

using namespace QtDataVisualization;

Spectogram::Spectogram(Q3DSurface *surface)
    : m_graph(surface)
{
    m_graph->activeTheme()->setType(Q3DTheme::Theme(1));
    m_graph->setOrthoProjection(true);
    m_graph->scene()->activeCamera()->setCameraPreset(Q3DCamera::CameraPresetDirectlyAbove);

    m_graph->setAxisX(new QValue3DAxis);
    m_graph->setAxisY(new QValue3DAxis);
    m_graph->setAxisZ(new QValue3DAxis);

    m_graph->axisX()->setRange(0, 99);
    m_graph->axisY()->setRange(0, 3);
    m_graph->axisZ()->setRange(0, 99);

    //m_graph->axisX()->setLabelFormat("%.2f");
    //m_graph->axisZ()->setLabelFormat("%.2f");
    m_graph->axisX()->setLabelAutoRotation(30);
    m_graph->axisY()->setLabelAutoRotation(90);
    m_graph->axisZ()->setLabelAutoRotation(30);


    m_graph->axisX()->setTitle(QStringLiteral("Frequenz"));
    m_graph->axisY()->setTitle(QStringLiteral("Alpha"));
    m_graph->axisZ()->setTitle(QStringLiteral("Cyclic Spectrum"));

    m_CyclicProxy  = new QSurfaceDataProxy();
    m_CyclicSeries = new QSurface3DSeries(m_CyclicProxy);

    m_fft_len   = 1024;
    m_alpha_len = 1024;

    init_data();
    //m_CyclicSeries->setFlatShadingEnabled(true);
    m_graph->addSeries(m_CyclicSeries);

    // Define Gradient
    QLinearGradient grAtoB(0, 0, 100, 100);
    grAtoB.setColorAt(0.0, Qt::darkBlue);
    grAtoB.setColorAt(0.5, Qt::yellow);
    grAtoB.setColorAt(1.0, Qt::darkRed);

    m_graph->seriesList().at(0)->setBaseGradient(grAtoB);
    m_graph->seriesList().at(0)->setColorStyle(Q3DTheme::ColorStyleRangeGradient);
}

Spectogram::~Spectogram()
{
    delete m_graph;
}

void Spectogram::init_data(){
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    //std::uniform_real_distribution<double> dis(0.0, 1.0);
    std::normal_distribution<float> dis(0,1);

    dataArray = new QSurfaceDataArray;
    dataArray->reserve(m_alpha_len);
    for (int i = 0 ; i < m_alpha_len ; i++) {
        QSurfaceDataRow *newRow = new QSurfaceDataRow(m_fft_len);
        int index = 0;
        for (int j = 0; j < m_fft_len; j++) {
            float x = j; // Frequenzen auf X-Achse
            float z = i; // Alphas auf y-Achse
            float y =  dis(gen);
            (*newRow)[index++].setPosition(QVector3D(x, y, z));
        }
        *dataArray << newRow;
    }
    m_CyclicProxy->resetArray(dataArray);
}

void Spectogram::update_data(){
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    //std::uniform_real_distribution<double> dis(0.0, 1.0);
    std::normal_distribution<float> dis(0,1);

    #pragma omp parallel for
    for (int i = 0 ; i < m_alpha_len ; i++) {
        for (int j = 0; j < m_fft_len; j++) {
            (*dataArray->at(i))[j].setY(dis(gen));
        }
    }

    m_CyclicProxy->arrayReset(); // Updates the Plot
}

void Spectogram::updatePlotData(){
    update_data();
}

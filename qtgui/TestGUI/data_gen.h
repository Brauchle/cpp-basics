#ifndef DATA_GEN_H
#define DATA_GEN_H

#include <random>

void gen_data(double *x, double *y, int p_size);

#endif // DATA_GEN_H

#version 330 core
layout (location = 0) in vec2 par_pos;
layout (location = 1) in vec2 par_speed;
layout (location = 2) in vec4 par_color;

out vec4 vertex_color;

void main()
{
    gl_Position = vec4(par_pos.x, par_pos.y, 0.0, 1.0);
    vertex_color = par_color;
    //vertex_color = vec4(par_speed, 0, 1.0);
    //vertex_color = vec4(1.0, 0,0, 1.0);
}

#version 330 core
out vec4 frag_color;

in vec2 tex_coord;
in vec4 fs_color;

uniform sampler2D ourTexture;

void main()
{
  vec4 texColor = texture(ourTexture, tex_coord);
  if(texColor.a < 0.1)
      discard;
  frag_color = fs_color * texColor;
}

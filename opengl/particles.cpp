#include "particles.h"

namespace param {
  const float cluster_attraction = 2500.6;
  const float p_view_angle       = 0.3; // * 2 * pi
  const float p_view_distance    = 100; // * 2 * pi
  const float p_p_rejection      = 200;
  const float p_speed_limit      = 500.0;
  const float p_assim_strength   = 0.001;
  const float p_speedup_free     = 30;
  const float p_wall_rejection   = 500;
}


Particles::Particles(){}

Particles::~Particles(){
}

void Particles::generate_particles(size_t number){
  m_storage.resize(number);
  std::cout << m_storage.size() << std::endl;

  w_limit = 1;
  h_limit = 1;
  // All Particle Properties will be initilized to a random value between -1 and 1
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<float> dis(-1.0, 1.0);
  std::uniform_real_distribution<float> dis_c(0.0, 1.0);

  for(Particle& e:m_storage){
    e.pos.x = dis(gen);
    e.pos.y = dis(gen);
    e.vel.x = dis(gen);
    e.vel.y = dis(gen);
    e.col.r = dis_c(gen);
    e.col.g = dis_c(gen);
    e.col.b = dis_c(gen);
    e.col.a = 1.0;
  }

  // Generate n_clusters
  n_clusters = 5;
  cluster_colors.resize(n_clusters);
  for (Color& e:cluster_colors) {
    e.r = dis_c(gen);
    e.g = dis_c(gen);
    e.b = dis_c(gen);
    e.a = 1.0;
  }

  cluster_speed.resize(n_clusters);
  for (vec2& e:cluster_speed) {
    e.x = 0;
    e.y = 0;
  }

  cluster.resize(n_clusters);
  for (vec2& e:cluster) {
    e.x = dis(gen);
    e.y = dis(gen);
  }

  m_c_storage.resize(m_storage.size());
  for (size_t i = 0; i < m_storage.size(); i++) {
    m_c_storage[i].c_dist.resize(n_clusters);
    m_c_storage[i].pos = &m_storage[i].pos;
  }

  cluster_elements.resize(n_clusters);
}

void* Particles::get_storage(){
  if(!m_storage.empty()){
    return (void*)&m_storage[0];
  }
  return nullptr;
}

size_t Particles::get_storage_size(){
  return m_storage.size() * sizeof(Particle);
}

size_t Particles::get_particle_size(){
  return sizeof(Particle);
}

size_t Particles::get_n_particles(){
  return m_storage.size();
}

void Particles::move(float dt){
  repulsion(dt);


  #pragma omp parallel for
  for (Particle& e : m_storage) {
    // Slows down a particle if necessary
    limit_speed(e);

    e.pos = e.pos + dt * e.vel;

    // Checks if a Particle leaves the screen and deals with it
    check_bounds(e);
  }
}

// void Particles::check_bounds(Particle& e){
//   if(e.pos.x < -1 ){ e.pos.x =  1; }
//   if(e.pos.x >  1 ){ e.pos.x = -1; }
//   if(e.pos.y < -1 ){ e.pos.y =  1; }
//   if(e.pos.y >  1 ){ e.pos.y = -1; }
// }

void Particles::check_bounds(Particle& e){
  // if(e.pos.x < -1 * w_limit ){ e.vel.x =  1 * abs(e.vel.x); }
  // if(e.pos.x >  1 * w_limit ){ e.vel.x = -1 * abs(e.vel.x); }
  // if(e.pos.y < -1 * h_limit ){ e.vel.y =  1 * abs(e.vel.y); }
  // if(e.pos.y >  1 * h_limit ){ e.vel.y = -1 * abs(e.vel.y); }

  // Wall Rejection
  if(e.pos.x < -1 * w_limit + param::p_view_distance ){
    e.vel.x = e.vel.x + param::p_wall_rejection;
  }
  if(e.pos.x >  1 * w_limit - param::p_view_distance ){
    e.vel.x = e.vel.x - param::p_wall_rejection;
  }
  if(e.pos.y < -1 * h_limit + param::p_view_distance ){
    e.vel.y = e.vel.y + param::p_wall_rejection;
  }
  if(e.pos.y >  1 * h_limit - param::p_view_distance ){
    e.vel.y = e.vel.y - param::p_wall_rejection;
  }
}

void Particles::repulsion(float dt){
  #pragma omp parallel for
  for (size_t i = 0; i < m_storage.size(); i++) {
    size_t in_view_count = 0;
    for (size_t j = 0; j < m_storage.size(); j++) {
      if(i!=j){
        float dist_ab = vec2_dist(m_storage[i].pos, m_storage[j].pos);

        vec2 p_vec = m_storage[j].pos - m_storage[i].pos;

        float angle = vec2_angle(m_storage[i].vel, p_vec);

        if( dist_ab < param::p_view_distance && angle < M_PI * param::p_view_angle){
          in_view_count++;

          // Left or Right?
          vec2 rej_vec;
          vec2_rejection(m_storage[i].vel, p_vec, rej_vec);
          float abs_rej = vec2_abs(rej_vec);

          // m_storage[i].vel = m_storage[i].vel + rej_vec / abs_rej * param::p_p_rejection;
          m_storage[i].vel = m_storage[i].vel - rej_vec / abs_rej * param::p_p_rejection;

          // Speed Assimiliation
          m_storage[i].vel = (1 - param::p_assim_strength) * m_storage[i].vel
                                + param::p_assim_strength  * m_storage[j].vel;
        }
      }
    }
    if(in_view_count == 0){
      m_storage[i].vel = m_storage[i].vel + param::p_speedup_free * m_storage[i].vel * dt;
    }
  }
}

void Particles::limit_speed(Particle& e){
  float abs_speed = vec2_abs(e.vel);
  if( abs_speed > param::p_speed_limit ){
    e.vel = e.vel * (param::p_speed_limit / abs_speed);
  }
}


void Particles::change_resolution(float w, float h){
  w_limit = w;
  h_limit = h;
}

void Particles::clustering(float dt){
  // Assign Random Cluster Centers
  // std::random_device rd;
  // std::mt19937 gen(rd());
  // std::uniform_real_distribution<float> dis(-1.0, 1.0);
  //
  // cluster.resize(n_clusters);
  // for (vec2& e:cluster) {
  //   e.x = dis(gen);
  //   e.y = dis(gen);
  // }
  // All Particle Properties will be initilized to a random value between -1 and 1
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<float> dis_c(-0.3, 0.3);
  // Generate n_clusters
  for (Color& e:cluster_colors) {
    e.r = e.r + dis_c(gen) * dt;
    e.g = e.g + dis_c(gen) * dt;
    e.b = e.b + dis_c(gen) * dt;
    e.a = 1.0;
  }


  for (size_t i = 0; i < 5; i++) {
    // Find Cluster with smallest distance
    for (ClusterData& p: m_c_storage) {
      //
      p.min_dist = 10;
      for (size_t i = 0; i < cluster.size(); i++) {
        float dist = vec2_dist(*p.pos, cluster[i]);
        p.c_dist[i] = dist;
        if(dist < p.min_dist){
          p.min_dist = dist;
          p.c_id = i;
        }
      }
    }


    // Assign new Cluster Centers
    for (size_t& e: cluster_elements) {
      e=0;
    }

    for (vec2& e: cluster) {
      e.x=0;
      e.y=0;
    }

    for (vec2& e: cluster_speed) {
      e.x=0;
      e.y=0;
    }

    for (ClusterData& p: m_c_storage) {
      cluster_elements[p.c_id]++;
      cluster[p.c_id] = cluster[p.c_id] + *p.pos;
    }
    for (size_t i = 0; i < cluster_speed.size(); i++) {
      cluster_speed[m_c_storage[i].c_id] = cluster_speed[m_c_storage[i].c_id] + m_storage[i].vel;
    }

    for (size_t i = 0; i < cluster.size(); i++) {
      if( cluster_elements[i] > 0){
        cluster[i] = cluster[i] / float( cluster_elements[i]);
        cluster_speed[i] = cluster_speed[i] / float( cluster_elements[i]);
      } else {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<float> dis(-1.0, 1.0);

        cluster[i].x = dis(gen);
        cluster[i].y = dis(gen);
      }
    }
  }

  // Assign Colors
  for (size_t i = 0; i < m_storage.size(); i++) {
    m_storage[i].col.r = cluster_colors[ m_c_storage[i].c_id ].r
                         * (1-m_c_storage[i].c_dist[m_c_storage[i].c_id]) / 2;
    m_storage[i].col.g = cluster_colors[ m_c_storage[i].c_id ].g
                         * (1-m_c_storage[i].c_dist[m_c_storage[i].c_id]) / 2;
    m_storage[i].col.b = cluster_colors[ m_c_storage[i].c_id ].b
                         * (1-m_c_storage[i].c_dist[m_c_storage[i].c_id]) / 2;

   // Assign Speed

    m_storage[i].vel = m_storage[i].vel + param::cluster_attraction * dt * cluster_speed[ m_c_storage[i].c_id ];
  }


}


void Particles::clustering_dbscan(float dt){
  // Assign Random Cluster Centers
  // std::random_device rd;
  // std::mt19937 gen(rd());
  // std::uniform_real_distribution<float> dis(-1.0, 1.0);
  //
  // cluster.resize(n_clusters);
  // for (vec2& e:cluster) {
  //   e.x = dis(gen);
  //   e.y = dis(gen);
  // }
  // All Particle Properties will be initilized to a random value between -1 and 1
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<float> dis_c(-0.9, 0.9);
  // Generate n_clusters
  for (Color& e:cluster_colors) {
    e.r = e.r + dis_c(gen) * dt;
    e.g = e.g + dis_c(gen) * dt;
    e.b = e.b + dis_c(gen) * dt;
    e.a = 1.0;
  }


  for (size_t i = 0; i < 5; i++) {
    // Find Cluster with smallest distance

    #pragma omp parallel for
    for (ClusterData& p: m_c_storage) {
      //
      p.min_dist = 10000;
      for (size_t i = 0; i < cluster.size(); i++) {
        float dist = vec2_dist(*p.pos, cluster[i]);
        p.c_dist[i] = dist;
        if(dist < p.min_dist){
          p.min_dist = dist;
          p.c_id = i;
        }
      }
    }


    // Assign new Cluster Centers
    for (size_t& e: cluster_elements) {
      e=0;
    }

    for (vec2& e: cluster) {
      e.x=0;
      e.y=0;
    }

    for (vec2& e: cluster_speed) {
      e.x=0;
      e.y=0;
    }

    for (ClusterData& p: m_c_storage) {
      cluster_elements[p.c_id]++;
      cluster[p.c_id] = cluster[p.c_id] + *p.pos;
    }
    for (size_t i = 0; i < cluster_speed.size(); i++) {
      cluster_speed[m_c_storage[i].c_id] = cluster_speed[m_c_storage[i].c_id] + m_storage[i].vel;
    }

    for (size_t i = 0; i < cluster.size(); i++) {
      if( cluster_elements[i] > 0){
        cluster[i] = cluster[i] / float( cluster_elements[i]);
        cluster_speed[i] = cluster_speed[i] / float( cluster_elements[i]);
      } else {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<float> dis(-1.0 * w_limit, 1.0 * h_limit);

        cluster[i].x = dis(gen);
        cluster[i].y = dis(gen);
      }
    }
  }

  // Assign Colors
  for (size_t i = 0; i < m_storage.size(); i++) {
    // For weighted colors
    float sum_dist = 0;
    for(float e : m_c_storage[i].c_dist){
      // sum_dist += e*e;
      // sum_dist += e;
      sum_dist += 1/e;
    }

    m_storage[i].col.r = 0;
    m_storage[i].col.g = 0;
    m_storage[i].col.b = 0;
    m_storage[i].col.a = 1.0;

    for (size_t j = 0; j < n_clusters; j++) {
      float scale = 1/m_c_storage[i].c_dist[j]/sum_dist;
      // float scale = 1 - m_c_storage[i].c_dist[j] / sum_dist;
      // float scale = (sum_dist - m_c_storage[i].c_dist[j] * m_c_storage[i].c_dist[j]) / sum_dist / 3;
      m_storage[i].col.r += cluster_colors[j].r * scale;
      m_storage[i].col.g += cluster_colors[j].g * scale;
      m_storage[i].col.b += cluster_colors[j].b * scale;
    }

   // Assign Speed
   vec2 tmp =  cluster[ m_c_storage[i].c_id ] - m_storage[i].pos;
   vec2_norm(tmp, tmp);
    m_storage[i].vel = m_storage[i].vel + param::cluster_attraction * dt * tmp;
     // m_storage[i].vel = m_storage[i].vel + 5 * dt * cluster_speed[ m_c_storage[i].c_id ];
  }


}

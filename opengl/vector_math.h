#ifndef __2D_VEC_MATH_LIB_BY_ME__
#define __2D_VEC_MATH_LIB_BY_ME__

#include <cmath>
#include <iostream>

struct vec2 {
  float x;
  float y;
};

// VEC VEC Operators
////////////////////////////////////////////////////////////////////////////////
vec2 operator+(vec2 a, const vec2& b);
vec2 operator-(vec2 a, const vec2& b);
vec2 operator*(vec2 a, const vec2& b);
vec2 operator/(vec2 a, const vec2& b);

// VEC Float Operators
////////////////////////////////////////////////////////////////////////////////
vec2 operator+(vec2 a, const float& b);
vec2 operator-(vec2 a, const float& b);
vec2 operator*(vec2 a, const float& b);
vec2 operator/(vec2 a, const float& b);

// Float Vec Operators
////////////////////////////////////////////////////////////////////////////////
vec2 operator+(const float& b, vec2 a);
vec2 operator-(const float& b, vec2 a);
vec2 operator*(const float& b, vec2 a);
vec2 operator/(const float& b, vec2 a);

// Other Functions
////////////////////////////////////////////////////////////////////////////////
float vec2_abs(vec2& vec);

float vec2_dist(vec2& vec_a, vec2& vec_b);

float vec2_dot(vec2& v1, vec2& v2);

void vec2_norm(vec2& vec, vec2& vec_normal);

void vec2_projection(vec2& ref, vec2& sec, vec2& out);

void vec2_rejection(vec2& ref, vec2& sec, vec2& out);

float vec2_angle(vec2& vec1, vec2& vec2);

#endif

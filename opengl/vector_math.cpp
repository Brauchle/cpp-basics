#include "vector_math.h"

// struct vec2 {
//   float x;
//   float y;
// };

// VEC VEC Operators
////////////////////////////////////////////////////////////////////////////////
vec2 operator+(vec2 a, const vec2& b){
  a.x += b.x;
  a.y += b.y;
  return a;
}

vec2 operator-(vec2 a, const vec2& b){
  a.x -= b.x;
  a.y -= b.y;
  return a;
}

vec2 operator*(vec2 a, const vec2& b){
  a.x *= b.x;
  a.y *= b.y;
  return a;
}

vec2 operator/(vec2 a, const vec2& b){
  a.x /= b.x;
  a.y /= b.y;
  return a;
}

// VEC Float Operators
////////////////////////////////////////////////////////////////////////////////
vec2 operator+(vec2 a, const float& b){
  a.x += b;
  a.y += b;
  return a;
}

vec2 operator-(vec2 a, const float& b){
  a.x -= b;
  a.y -= b;
  return a;
}

vec2 operator*(vec2 a, const float& b){
  a.x *= b;
  a.y *= b;
  return a;
}

vec2 operator/(vec2 a, const float& b){
  a.x /= b;
  a.y /= b;
  return a;
}

// Float Vec Operators
////////////////////////////////////////////////////////////////////////////////
vec2 operator+(const float& b, vec2 a){
  a.x += b;
  a.y += b;
  return a;
}

vec2 operator-(const float& b, vec2 a){
  a.x -= b;
  a.y -= b;
  return a;
}

vec2 operator*(const float& b, vec2 a){
  a.x *= b;
  a.y *= b;
  return a;
}

vec2 operator/(const float& b, vec2 a){
  a.x /= b;
  a.y /= b;
  return a;
}

// Other Functions
////////////////////////////////////////////////////////////////////////////////
float vec2_abs(vec2& vec){
  return std::sqrt( std::pow(vec.x, 2) + std::pow(vec.y, 2) );
}

float vec2_dist(vec2& vec_a, vec2& vec_b){
  return std::sqrt( std::pow(vec_a.x - vec_b.x, 2) + std::pow(vec_a.y - vec_b.y, 2) );
}

float vec2_dot(vec2& v1, vec2& v2){
  return v1.x*v2.x + v1.y*v2.y;
}

void vec2_norm(vec2& vec, vec2& vec_normal){
  float fac = vec2_abs(vec);
  vec_normal = vec / fac;
}

void vec2_projection(vec2& ref, vec2& sec, vec2& out){
  vec2 ref_norm;
  vec2_norm(ref, ref_norm);
  float a1 = vec2_dot(ref, sec) / vec2_abs(ref);

  out = ref_norm * a1;
}

void vec2_rejection(vec2& ref, vec2& sec, vec2& out){
  vec2 proj;
  vec2_projection(ref, sec, proj);
  out = sec - proj;
}

float vec2_angle(vec2& vec1, vec2& vec2){
  float arg = vec2_dot(vec1, vec2) / vec2_abs(vec1) / vec2_abs(vec2);
  if( arg > 1)  {return 0; }
  if( arg < -1) {return M_PI; }
  // Rounding Erros can cause arg to become slightly out of bounds.... meeheheheh
  return std::acos( arg );
}

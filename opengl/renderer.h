#ifndef __RENDERER_OPENGL__
#define __RENDERER_OPENGL__
// STD Stuff
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

// OpenGL Helper Stuff
#include <GL/glew.h>

// SDL STUFF
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

// Texture Stuff

#include "shader.h"

class Renderer
{
public:
  Renderer();
  ~Renderer();

  void refresh();
  void load_texture();
  void load_vertex_array_buffer();
  void load_vertex_buffer(void* vertices, size_t n_vals);

  void change_resolution(size_t w, size_t h);

  void toggle_fullscreen();

  SDL_Window* window;
  SDL_GLContext context;

  Shader* shader;

  GLuint vertex_array_buffer;
  GLuint vertex_buffer;

  GLuint uniform_scaling;
  float scaling[2];


  GLuint texture;
  GLuint n_verts;

  bool is_fullscreen;
};

#endif

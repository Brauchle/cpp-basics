// std
#include <iostream>
#include <chrono>

#include "renderer.h"
#include "particles.h"

int main(int argc, char *argv[])
{
  // SDL INIT
  //////////////////////////////////////////////////////////////////////////////
  SDL_Init(SDL_INIT_VIDEO);

  Renderer renderer;
  // Generiert zufällige Partikel
  Particles particles;
  particles.generate_particles(1000);

  // Warum auch immer ist hier die Reihenfolge wichtig
  renderer.load_vertex_array_buffer();
  renderer.load_vertex_buffer(particles.get_storage(), particles.get_n_particles());
  renderer.load_texture();

  // Setup set_resolution
  renderer.change_resolution(800, 600);
  particles.change_resolution(800, 600);

  // MAIN LOOP
  //////////////////////////////////////////////////////////////////////////////
  std::chrono::time_point<std::chrono::high_resolution_clock> tp_start, tp_end;
  auto s_time = std::chrono::duration_cast<std::chrono::duration<double>>(tp_end - tp_start);

  tp_end   = std::chrono::high_resolution_clock::now();
  tp_start = std::chrono::high_resolution_clock::now();

  SDL_Event event;
  bool keep_going = true;
  while (keep_going)
  {
      // Time Goes Gy
      tp_end   = std::chrono::high_resolution_clock::now();
      s_time   = std::chrono::duration_cast<std::chrono::duration<double>>(tp_end - tp_start);
      tp_start = std::chrono::high_resolution_clock::now();

      // Event Handler
      if (SDL_PollEvent(&event))
      {
          // Close Window Button
          if (event.type == SDL_QUIT) break;

          if (event.type == SDL_WINDOWEVENT) {
            switch (event.window.event) {
              case SDL_WINDOWEVENT_RESIZED:
                renderer.change_resolution(event.window.data1, event.window.data2);
                particles.change_resolution(event.window.data1, event.window.data2);

                std::cout << event.window.data1 << " event b " << event.window.data2 << std::endl;
                break;
              case SDL_WINDOWEVENT_SIZE_CHANGED:
                renderer.change_resolution(event.window.data1, event.window.data2);
                particles.change_resolution(event.window.data1, event.window.data2);

                std::cout << event.window.data1 << " event a " << event.window.data2 << std::endl;
                break;
              // case SDL_WINDOWEVENT_CLOSE:
              //     SDL_Log("Window %d closed", event->window.windowID);
              //     break;
              default:
                break;
            }
        }

          if (event.type == SDL_KEYDOWN){
            switch( event.key.keysym.sym ){
                  case SDLK_ESCAPE:
                      keep_going = false;
                      break;
                  case SDLK_RETURN:
                      renderer.toggle_fullscreen();
                      break;
                  default:
                      break;
              }
          }
      }
      // Graphics Stuff
      particles.move(s_time.count());
      particles.clustering_dbscan(s_time.count());
      // particles.clustering(s_time.count());
      renderer.load_vertex_buffer(particles.get_storage(), particles.get_n_particles());
      renderer.refresh();
  }

  // Destruction
  //////////////////////////////////////////////////////////////////////////////

  SDL_Quit();
  return 0;
}

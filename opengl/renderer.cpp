#include "renderer.h"
#define STB_IMAGE_IMPLEMENTATION
#include "extern/stb_image.h"

Renderer::Renderer(){
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
  // Stencil Buffer
  SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

  // Blending / Alpha / Transparency
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  window = SDL_CreateWindow("OpenGL", 100, 100, 400, 400, SDL_WINDOW_OPENGL);
  context = SDL_GL_CreateContext(window);

  SDL_SetWindowResizable(window, SDL_TRUE);

  // GLEW INIT
  glewExperimental = GL_TRUE;
  glewInit();

  // SHADER INIT
  shader = new Shader("shaders/particle_ar.vs", "shaders/particle.gs", "shaders/particle.fs");
  shader->use();

  // Connect to uniform
  scaling[0] = 1;
  scaling[1] = 1;
  uniform_scaling = glGetUniformLocation(shader->ID, "scaling");
  glUniform2f(uniform_scaling, scaling[0], scaling[1]);

  change_resolution(800, 400);
  is_fullscreen = false;
}

Renderer::~Renderer(){
  // Destroy OpenGL Context
  SDL_GL_DeleteContext(context);
  delete shader;
}

void Renderer::load_texture(){
  // Texture
  glGenTextures(1, &texture);

  glBindTexture(GL_TEXTURE_2D, texture);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  int width, height, n_channels;
  unsigned char *data = stbi_load("textures/particle3.png", &width, &height, &n_channels, 0);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
  glGenerateMipmap(GL_TEXTURE_2D);

  stbi_image_free(data);

  shader->use();
  shader->setInt("texture_particle", 0);
}

void Renderer::load_vertex_array_buffer(){
  glGenVertexArrays(1, &vertex_array_buffer);
  glBindVertexArray(vertex_array_buffer);
}

void Renderer::load_vertex_buffer(void* vertices, size_t n_vals){
  // vals_per_vert - gibt an wieviele floats zu einem vertex gehören / Datenstruktur
  n_verts = n_vals;
  glGenBuffers(1, &vertex_buffer);
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 8 * n_vals, vertices, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(2*sizeof(float)));
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(4*sizeof(float)));
  glEnableVertexAttribArray(2);
  glBindVertexArray(0);
}

void Renderer::change_resolution(size_t w, size_t h){
  // SDL_SetWindowSize(window, w, h);
  // float ar = x/y;
  scaling[0] = 1/float(w);
  scaling[1] = 1/float(h);
  glUniform2f(uniform_scaling, scaling[0], scaling[1]);

  glViewport( 0, 0, w, h);
}

void Renderer::toggle_fullscreen(){
  if(is_fullscreen){
    SDL_SetWindowFullscreen(window, 0);
    is_fullscreen = false;
  } else {
    SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    is_fullscreen = true;
  }
}

void Renderer::refresh(){
  // Clear Screen
  glClearColor(0.0f, 0.0f, 0.0f, 0.01f);
  glClear(GL_COLOR_BUFFER_BIT);

  // bind textures
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texture);

  // Draw Stuff
  shader->use();
  glBindVertexArray(vertex_array_buffer);
  glDrawArrays(GL_POINTS, 0, n_verts);

  SDL_GL_SwapWindow(window);
}

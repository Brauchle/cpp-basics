#include <iostream>

class TestClass {
public:
  int n_ints;
  int *stored_int;
  TestClass (){
    std::cout << " . Constructor" << std::endl;
    n_ints = 0;
    stored_int = nullptr;
  }

  ~TestClass (){
    std::cout << " . Destructor" << std::endl;
    delete[] stored_int;
  }

  TestClass (int n){
    std::cout << " . Constructor" << std::endl;
    stored_int = new int[n];
    n_ints = n;
  }

  // Copy Constructor
  TestClass(const TestClass& a){
    std::cout << " . Copy Constructor" << std::endl;
    n_ints = a.n_ints;

    stored_int = new int[n_ints];

    for (size_t i = 0; i < a.n_ints; ++i) {
      stored_int[i] = a.stored_int[i];
    }
  }
  // Move Constructor
  TestClass(TestClass&& a){
    std::cout << " . Move Constructor" << std::endl;

    n_ints = a.n_ints;
    stored_int = a.stored_int;

    a.stored_int = nullptr;
    a.n_ints = 0;
  }

  // Copy Assignment
  TestClass& operator=(const TestClass& a){
    std::cout << " . Copy Assignment" << std::endl;
    delete[] stored_int;

    n_ints = a.n_ints;
    stored_int = new int[n_ints];
    for (size_t i = 0; i < a.n_ints; ++i) {
      stored_int[i] = a.stored_int[i];
    }

    return *this;
  }
  // Move Assignment
  TestClass& operator=(TestClass&& a){
    std::cout << " . Move Assignment" << std::endl;
    std::cout << stored_int << std::endl;
    delete[] stored_int;
    std::cout << "TEST TEST" << std::endl;
    std::cout << stored_int << std::endl;
    stored_int = a.stored_int;
    std::cout << stored_int << std::endl;
    n_ints = a.n_ints;
    a.stored_int = nullptr;
    a.n_ints = 0;
    std::cout << stored_int << std::endl;

    return *this;
  }

};

class TestClass2{
public:
  int n_ints;
  TestClass2 (){
    std::cout << " . Constructor" << std::endl;
    n_ints = 0;
  }

  ~TestClass2 (){
    std::cout << " . Destructor" << std::endl;
  }

  TestClass2 (int n){
    std::cout << " . Constructor" << std::endl;
    n_ints = n;
  }

  // Copy Constructor
  TestClass2(const TestClass2& a){
    std::cout << " . Copy Constructor" << std::endl;
    n_ints = a.n_ints;
  }
  // Move Constructor
  TestClass2(TestClass2&& a){
    std::cout << " . Move Constructor" << std::endl;

    n_ints = a.n_ints;

    a.n_ints = 0;
  }

  // Copy Assignment
  TestClass2& operator=(const TestClass2& a){
    std::cout << " . Copy Assignment" << std::endl;

    n_ints = a.n_ints;

    return *this;
  }
  // Move Assignment
  TestClass2& operator=(TestClass2&& a){
    std::cout << " . Move Assignment" << std::endl;

    n_ints = a.n_ints;
    a.n_ints = 0;

    return *this;
  }

};

void test_func( TestClass a ){
  int b;
  b = b*b*b;
}

void test_func_ref( TestClass& a ){
  int b;
  b = b*b*b;
}

void test_func_p( TestClass *a ){
  int b;
  b = b*b*b;
}

void test_func_refref( TestClass&& a ){
  int b;
  b = b*b*b;
}

TestClass return_TestClass(){ return TestClass(10); }
TestClass return_TestClass(TestClass a){ return a; }
TestClass return_TestClass_refref(TestClass&& a){ return std::move(a); }
TestClass return_a(){ TestClass a(10); return a; }

TestClass2 get_2_con(int n){return TestClass2(n);}
TestClass2 get_2_con_2(int n){TestClass2 test(n); return test; }
TestClass2 get_2_con_ass(int n){TestClass2 test; test.n_ints = n; return test; }

// Main Function
////////////////////////////////////////////////////////////////////////////////
int main() {
  {
    TestClass a(10);
    new(&a) TestClass(30);
    new(&a) TestClass(30);
    new(&a) TestClass(30);
  }
  // Constructor
  //////////////////////////////////////////////////////////////////////////////
  std::cout << "\n# Test Constructor\n";
  std::cout << "TestClass a(10);" << std::endl;
  {
    TestClass a(10);
    std::cout << " .\n";
  }

  std::cout << "TestClass b(a);" << std::endl;
  {
    TestClass a1(10);
    TestClass a2(a1);
    std::cout << " .\n";
  }

  std::cout << "TestClass b(std::move(a));" << std::endl;
  {
    TestClass a(10);
    TestClass b(std::move(a));
    std::cout << " .\n";
  }

  std::cout << "\n";



  // Copy
  //////////////////////////////////////////////////////////////////////////////
  std::cout << "\n# Test Copy\n";
  std::cout << "TestClass b = a;" << std::endl;
  {
    TestClass a(10);
    TestClass b = a;
    std::cout << " .\n";
  }
  std::cout << "TestClass b = std::move(a);" << std::endl;
  {
    TestClass a(10);
    TestClass b = std::move(a);
    std::cout << " .\n";
  }

  std::cout << "\n";



  // Call Function
  //////////////////////////////////////////////////////////////////////////////
  std::cout << "\n# Test Function Call\n";
  std::cout << "test_func(a);" << std::endl;
  {
    TestClass a(10);
    test_func(a);
    std::cout << " .\n";
  }

  std::cout << "test_func(TestClass(10));" << std::endl;
  {
    test_func(TestClass(10));
    std::cout << " .\n";
  }

  std::cout << "test_func(std::move(a));" << std::endl;
  {
    TestClass a(10);
    test_func(std::move(a));
    std::cout << " .\n";
  }

  std::cout << "test_func_ref(a);" << std::endl;
  {
    TestClass a(10);
    test_func_ref(a);
    std::cout << " .\n";
  }

  std::cout << "test_func_p(a);" << std::endl;
  {
    TestClass a(10);
    test_func_p(&a);
    std::cout << " .\n";
  }

  std::cout << "test_func_refref(std::move(a));" << std::endl;
  {
    TestClass a(10);
    test_func_refref(std::move(a));
    std::cout << " .\n";
  }

  std::cout << "\n";



  // R Value
  //////////////////////////////////////////////////////////////////////////////
  std::cout << "\n# Test Return Value\n";
  std::cout << "TestClass a = return_TestClass();" << std::endl;
  {
    TestClass a = return_TestClass();
    std::cout << " .\n";
  }

  std::cout << "a = return_TestClass()" << std::endl;
  {
    TestClass a;
    a = return_TestClass();
    std::cout << " .\n";
  }

  std::cout << "return_TestClass(a);" << std::endl;
  {
    TestClass a;
    return_TestClass(a);
    std::cout << " .\n";
  }

  std::cout << "return_TestClass(std::move(a));" << std::endl;
  {
    TestClass a;
    return_TestClass(std::move(a));
    std::cout << " .\n";
  }

  std::cout << "a = return_TestClass(a);" << std::endl;
  {
    TestClass a;
    a = return_TestClass(a);
    std::cout << " .\n";
  }

  std::cout << "a = return_TestClass(std::move(a));" << std::endl;
  {
    TestClass a(10);
    a = return_TestClass(std::move(a));
    std::cout << " .\n";
  }

  std::cout << "a = return_TestClass_refref(std::move(a));" << std::endl;
  {
    TestClass a(10);
    a = return_TestClass_refref(std::move(a));
    std::cout << " .\n";
  }

  std::cout << "a = return_b();" << std::endl;
  {
    TestClass a;
    a = return_a();
    std::cout << " .\n";
  }

  std::cout << "TestClass a = return_b();" << std::endl;
  {
    TestClass a = return_a();
    std::cout << " .\n";
  }

  std::cout << "TestClass a = return_TestClass()" << std::endl;
  {
    TestClass a = return_TestClass();
    std::cout << " .\n";
  }

  std::cout << "\n";

  std::cout << "\n";
  {
    TestClass2 a = get_2_con(10); //{return TestClass2(n);}
  }
  {
    TestClass2 a = get_2_con_2(10); //{TestClass2 test(n); return test; }
  }
  {
    TestClass2 get_2_con_ass(10);//{TestClass2 test; test.n_ints = n; return test; }
  }

  std::cout << "\n";

  std::cout << "MOVESTUFF INTO ITSELF\n";
  {
    TestClass a(10); //{return TestClass2(n);}
    a = std::move(a);
  }



  return 0;
}

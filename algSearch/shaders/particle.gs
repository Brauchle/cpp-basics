#version 330 core
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

uniform vec2 scaling;

in vec4 vertex_color[];
out vec2 tex_coord;
out vec4 fs_color;

void gen_particle(vec4 position)
{
    fs_color = vertex_color[0];
    //vertex_color = vec4(1.0, 1.0, 0.0, 1.0);
    float particle_size = 2 * scaling.y;

    float stretch = scaling.x / scaling.y;

    gl_Position = position + vec4(-particle_size * stretch , -particle_size, 0.0, 0.0);
    tex_coord = vec2(0,0);
    EmitVertex();

    gl_Position = position + vec4(-particle_size * stretch, particle_size, 0.0, 0.0);
    tex_coord = vec2(0,1);
    EmitVertex();

    gl_Position = position + vec4( particle_size * stretch, -particle_size, 0.0, 0.0);
    tex_coord = vec2(1,0);
    EmitVertex();

    gl_Position = position + vec4( particle_size * stretch, particle_size, 0.0, 0.0);
    tex_coord = vec2(1,1);
    EmitVertex();

    EndPrimitive();
}

void main() {
    gen_particle(gl_in[0].gl_Position);
}

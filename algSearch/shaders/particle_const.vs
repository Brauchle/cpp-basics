#version 330 core
layout (location = 0) in vec2 par_pos;
layout (location = 1) in vec4 par_color;

uniform vec2 scaling;

out vec4 vertex_color;

void main()
{
    gl_Position = vec4(par_pos.x*0.9, par_pos.y*0.9, 0.0, 1.0);
    vertex_color = par_color;
    //vertex_color = vec4(par_speed, 0, 1.0);
    //vertex_color = vec4(1.0, 0,0, 1.0);
}

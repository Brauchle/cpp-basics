#include "searchalgorithm.h"

Searchalgorithm::Searchalgorithm(){}
Searchalgorithm::~Searchalgorithm(){}



void Searchalgorithm::init(Algorithms alg, size_t pop_size, size_t constellation_size){
  // Init Parameters
  used_algorithm          = alg;
  alg_population_size     = pop_size;
  alg_constellation_size  = constellation_size;

  sa_temp  = 1;
  sa_count = 0;
  sa_speed = 0.01;
  sa_prop_drop = 2;
  sa_step_size = 0.1;

  ga_n_survivors  = 3;
  ga_n_dead       = pop_size - ga_n_survivors;
  ga_mut_chance   = 0.25;
  ga_mut_strength = 0.1;

  ps_speed = 0.1;

  ps_speed_l = 0.01;
  ps_speed_g = 0.02;
  ps_inertia = 1;

  ps_speed_ul = 1;
  ps_speed_ll = 0.01;

  // Initialize Random Generator
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<float> disAmp(0, 1.0);
  std::uniform_real_distribution<float> dis(-1.0, 1.0);

  constellations.resize(pop_size);
  for (Constellation& e:constellations) {
    e.cpoints.resize(constellation_size);
    e.bpoints.resize(constellation_size);
    e.spoints.resize(constellation_size);

    // Random Initialization
    for (size_t i = 0; i < alg_constellation_size; i++) {
      e.cpoints[i] = disAmp(gen) * std::exp( std::complex<float>(0, M_PI * dis(gen)) );
      e.bpoints[i] = e.cpoints[i];
      e.spoints[i] = ps_speed * disAmp(gen) * std::exp( std::complex<float>(0, M_PI * dis(gen)) );
    }
  }



  best.rating = 0;
  best.min_dist = 0;
  best.mean_dist = 0;
}

void Searchalgorithm::reset(){
  sa_temp  = 1;
  sa_count = 0;

  // Initialize Random Generator
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<float> disAmp(0, 1.0);
  std::uniform_real_distribution<float> dis(-1.0, 1.0);

  for (Constellation& e:constellations) {
    // Random Initialization
    e.rating = 0;
    for (size_t i = 0; i < alg_constellation_size; i++) {
      e.cpoints[i] = disAmp(gen) * std::exp( std::complex<float>(0, M_PI * dis(gen)) );
      e.bpoints[i] = e.cpoints[i];
      e.spoints[i] = ps_speed * disAmp(gen) * std::exp( std::complex<float>(0, M_PI * dis(gen)) );
    }
  }

  best.rating = 0;
  best.min_dist = 0;
  best.mean_dist = 0;
}

float Searchalgorithm::get_x(size_t con, size_t pnt){
  return constellations[con].cpoints[pnt].real();
}

float Searchalgorithm::get_y(size_t con, size_t pnt){
  return constellations[con].cpoints[pnt].imag();
}

float Searchalgorithm::get_x_best(size_t pnt){
  return best.cpoints[pnt].real();
}

float Searchalgorithm::get_y_best(size_t pnt){
  return best.cpoints[pnt].imag();
}

void Searchalgorithm::rate_single_constellation(Constellation& e){
  // distance between the points is a meassure for the AWGN Channel
  // Amplitude maximum is 1 that means the maximum distance is 2
  // Minimum distance? Average Distance? Not sure

  float min_dist  = 2;
  float mean_dist = 0;

  for (size_t i = 0; i < alg_constellation_size - 1; i++) {
    for (size_t j = i+1; j < alg_constellation_size; j++) {
      float dist = std::sqrt( std::pow(e.cpoints[i].real() - e.cpoints[j].real(), 2)
                            + std::pow(e.cpoints[i].imag() - e.cpoints[j].imag(), 2) );
      mean_dist += dist;

      if(dist < min_dist){
        min_dist = dist;
      }
    }
  }
  mean_dist = mean_dist / alg_constellation_size;

  e.rating = min_dist * 50 + mean_dist;
  e.min_dist = min_dist;
  e.mean_dist = mean_dist;
  //e.rating = min_dist;
}

void Searchalgorithm::rate_constellations(){
  // distance between the points is a meassure for the AWGN Channel
  // Amplitude maximum is 1 that means the maximum distance is 2
  // Minimum distance? Average Distance? Not sure
  float old_rating = 0;
  for(Constellation&e : constellations){
    old_rating = e.rating;
    rate_single_constellation(e);

    if(old_rating < e.rating){
      for (size_t i = 0; i < alg_constellation_size; i++) {
        e.bpoints[i] = e.cpoints[i];
      }
    }

    if(e.rating > best.rating){
      best = e;
    }

  }
}

void Searchalgorithm::update(){

  switch (used_algorithm) {
    case Algorithms::Random:
      up_random();
      break;

    case Algorithms::Genetic:
      up_genetic();
      break;

    case Algorithms::SimulatedAnnealing:
      up_simulated_annealing();
      break;

    case Algorithms::ParticleSwarm:
      up_particle_swarm();
      break;

    default:
      up_random();
      break;
  }

  rate_constellations();

}

void Searchalgorithm::up_random(){
  // Initialize Random Generator
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<float> disAmp(0, 1.0);
  std::uniform_real_distribution<float> dis(-1.0, 1.0);

  for (Constellation& e:constellations) {
    for(std::complex<float>& f: e.cpoints){
      f = disAmp(gen) * std::exp( std::complex<float>(0, M_PI * dis(gen)) );
    }
  }
}

void Searchalgorithm::up_genetic(){
  // selection, sort the constellation by rating
  // sorts the vector, smallest fitness elements at the beginning
  std::sort(constellations.begin(), constellations.end(), [](Constellation& a, Constellation& b){
    return a.rating < b.rating;
  });

  // reproduce(Population& pop, int n){
  std::random_device rd;  //Will be used to obtain a seed for the random number engine
  std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_int_distribution<> dis(0, ga_n_survivors - 1);

  #pragma omp parallel for
  for (size_t i = 0; i < ga_n_dead; i++) {
    int p1 = dis(gen);
    int p2 = dis(gen);
    while(p1 == p2){
      p2 = dis(gen);
    }

    // Reproduction split at half of the genes (genes are symbols in our case)
    for (size_t j = 0; j < alg_constellation_size; j++) {
      if(i < alg_constellation_size/2 ){
        constellations[i].cpoints[j] = constellations[ga_n_dead + p1].cpoints[j];
      } else {
        constellations[i].cpoints[j] = constellations[ga_n_dead + p2].cpoints[j];
      }
    }
  }

  // )mutatePopulation& pop, int n, float chance){
  std::uniform_real_distribution<float> dis_g(0.0, 1.0);
  std::uniform_int_distribution<> dis_m(0, alg_constellation_size - 1);
  std::normal_distribution<float> dis_inc(0, ga_mut_strength);

  #pragma omp parallel for
  for (size_t i = 0; i < ga_n_dead; i++) {
    if( ga_mut_chance > dis_g(gen) ) {
      int gene = dis_m(gen);

      constellations[i].cpoints[gene] = constellations[i].cpoints[gene] + std::complex<float>(dis_inc(gen), dis_inc(gen));
      if(std::abs(constellations[i].cpoints[gene])>1){
        constellations[i].cpoints[gene] = constellations[i].cpoints[gene] / std::abs(constellations[i].cpoints[gene]);
      }
    }
  }

}

void Searchalgorithm::up_simulated_annealing(){

  std::random_device rd;  //Will be used to obtain a seed for the random number engine
  std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()

  // )mutatePopulation& pop, int n, float chance){
  std::uniform_real_distribution<float> dis_amp(0.0, 1.0);
  std::uniform_real_distribution<float> dis_ph(-1, 1);

  Constellation new_const;
  new_const.cpoints.resize(alg_constellation_size);
  for (Constellation& e:constellations) {
    // Move Constellation
    for (size_t i = 0; i < alg_constellation_size; i++) {
      new_const.cpoints[i] = sa_step_size * dis_amp(gen) * std::exp( std::complex<float>(0, M_PI * dis_ph(gen)) ) + e.cpoints[i];
      if(abs(new_const.cpoints[i]) > 1){
        new_const.cpoints[i] = new_const.cpoints[i] / abs(new_const.cpoints[i]) ;
      }
    }
    rate_single_constellation(new_const);

    // Keep constellation if it is better or based on chance
    if(e.rating < new_const.rating){
      e = new_const;
    } else if (dis_amp(gen) < (sa_temp * (std::exp(-1 * sa_prop_drop * (new_const.rating - e.rating) ))) ) {
      e = new_const;
    }

  }

  sa_temp = std::exp( -1 * sa_speed * sa_count);
  sa_count += 1;
}

void Searchalgorithm::up_particle_swarm(){

    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()

    // )mutatePopulation& pop, int n, float chance){
    std::uniform_real_distribution<float> dis_amp(0.0, 1.0);
    std::uniform_real_distribution<float> dis_ph(-1, 1);

    for (Constellation& e:constellations) {

      // Move Constellation
      for (size_t i = 0; i < alg_constellation_size; i++) {
        // Update ps_speed, local best
        if( abs(e.spoints[i]) > ps_speed_ll ){
          e.spoints[i] = e.spoints[i] * ps_inertia;
        }
        // Update ps_speed, local best
        e.spoints[i] = e.spoints[i] + ps_speed_l * dis_amp(gen) * (e.bpoints[i] - e.cpoints[i]);
        // Update ps_speed, global best
        e.spoints[i] = e.spoints[i] + ps_speed_g * dis_amp(gen) * (best.cpoints[i] - e.cpoints[i]);

        // Move Constellation
        e.cpoints[i] = e.cpoints[i] + e.spoints[i];
        if(abs(e.cpoints[i]) > 1){
          e.spoints[i] = -1.0f * e.spoints[i];
          e.cpoints[i] = e.cpoints[i] / abs(e.cpoints[i]) ;
        }
      }
      ps_aplly_speed_limit(e);
    }
}

void Searchalgorithm::ps_aplly_speed_limit(Constellation& e){
  float tmp_speed = 0;
  for (size_t i = 0; i < alg_constellation_size; i++) {
    tmp_speed += std::abs( e.spoints[i] * std::conj(e.spoints[i]) );
  }
  tmp_speed = std::sqrt(tmp_speed);
  if (tmp_speed > ps_speed_ul) {
    float change_speed = ps_speed_ul / tmp_speed;

    for (size_t i = 0; i < alg_constellation_size; i++) {
      e.spoints[i] = e.spoints[i] * change_speed;
    }
  } else if (tmp_speed < ps_speed_ll) {
    float change_speed = ps_speed_ll / tmp_speed;

    for (size_t i = 0; i < alg_constellation_size; i++) {
      e.spoints[i] = e.spoints[i] * change_speed;
    }
  }
}

void Searchalgorithm::cout_rating(){
  std::cout << "Min Dist:  " << best.min_dist << std::endl;
  std::cout << "Mean Dist: " << best.mean_dist << std::endl << std::endl;
}

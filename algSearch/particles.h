#ifndef __PARTICLES_CLASS__
#define __PARTICLES_CLASS__
// STD Stuff
#include <cstddef>
#include <random>
#include <cmath>
#include <vector>

#include "vector_math.h"

struct Color{
  float r;
  float g;
  float b;
  float a;
};

struct Particle{
  vec2 pos;
  Color col;
};


class Particles
{
public:
  Particles();
  ~Particles();

  void* get_storage();
  size_t get_storage_size();
  size_t get_particle_size();
  size_t get_n_particles();

  std::vector<Particle> m_storage;

  void generate_particles(size_t number);
  void change_resolution(float w, float h);
  void change_color(float bright);

private:
  float w_limit, h_limit;
};

#endif

// std
#include <iostream>
#include <chrono>

#include "renderer.h"
#include "particles.h"

#include "searchalgorithm.h"

#include "helperfunctions.h"

int main(int argc, char *argv[])
{
  print_manual();

  size_t algorithm_id = 0;
  size_t constellation_size = 7;
  size_t population_size    = 10;

  switch (argc) {
    case 5:
    case 4:
      population_size = std::atoi(argv[3]);
    case 3:
      constellation_size = std::atoi(argv[2]);
    case 2:
      algorithm_id = std::atoi(argv[1]);
      break;
    default:

      break;
  }

  // Search ALgorithm INIT
  Searchalgorithm algorithm;

  std::cout << "Method: ";
  switch (algorithm_id) {
    case Searchalgorithm::Algorithms::Random:
      std::cout << "Random Search" << std::endl;
      population_size = 1;
      algorithm.init(Searchalgorithm::Algorithms::Random, population_size, constellation_size);
      break;

    case Searchalgorithm::Algorithms::SimulatedAnnealing:
      std::cout << "Simulated Annealing" << std::endl;
      population_size = 1;
      algorithm.init(Searchalgorithm::Algorithms::SimulatedAnnealing, population_size, constellation_size);
      break;

    case Searchalgorithm::Algorithms::Genetic:
      std::cout << "Genetic Algorithm" << std::endl;
      algorithm.init(Searchalgorithm::Algorithms::Genetic, population_size, constellation_size);
      break;

    case Searchalgorithm::Algorithms::ParticleSwarm:
      std::cout << "Particle Swarm" << std::endl;
      algorithm.init(Searchalgorithm::Algorithms::ParticleSwarm, population_size, constellation_size);
      break;

    default:
      population_size = 1;
      algorithm.init(Searchalgorithm::Algorithms::Random, population_size, constellation_size);
  }

  std::cout << "\nConstellation Size: " << constellation_size;
  std::cout << "\nPopulation Size: " << population_size;
  std::cout << std::endl;

  algorithm.rate_constellations();

  size_t num_particles      = constellation_size * (population_size + 1);
  size_t best_particle      = population_size;



  Color best_color;
  best_color.r = 1;
  best_color.g = 1;
  best_color.b = 1;
  best_color.a = 1;

  // Generiert zufällige Partikel
  Particles particles;
  particles.generate_particles(num_particles);


  // Update points
  for (size_t i = 0; i < population_size; i++) {
    /* code */
    for (size_t j = 0; j < constellation_size; j++) {
      particles.m_storage[ i*constellation_size + j ].pos.x = algorithm.get_x(i,j);
      particles.m_storage[ i*constellation_size + j ].pos.y = algorithm.get_y(i,j);
      particles.m_storage[ i*constellation_size + j ].col   = particles.m_storage[ i*constellation_size].col;
    }
  }

  // Best Particle
  for (size_t j = 0; j < constellation_size; j++) {
    particles.m_storage[ best_particle * constellation_size + j ].pos.x = algorithm.get_x_best(j);
    particles.m_storage[ best_particle * constellation_size + j ].pos.y = algorithm.get_y_best(j);
    particles.m_storage[ best_particle * constellation_size + j ].col   = best_color;
  }


  // SDL INIT
  //////////////////////////////////////////////////////////////////////////////
  SDL_Init(SDL_INIT_VIDEO);

  Renderer renderer;

  // Warum auch immer ist hier die Reihenfolge wichtig
  renderer.load_vertex_array_buffer();
  renderer.load_vertex_buffer(particles.get_storage(), particles.get_n_particles());
  renderer.load_texture();

  // Setup set_resolution
  renderer.change_resolution(800, 800);
  particles.change_resolution(800, 800);

  // MAIN LOOP
  //////////////////////////////////////////////////////////////////////////////
  std::chrono::time_point<std::chrono::high_resolution_clock> tp_start, tp_end;
  auto s_time = std::chrono::duration_cast<std::chrono::duration<double>>(tp_end - tp_start);

  tp_end   = std::chrono::high_resolution_clock::now();
  tp_start = std::chrono::high_resolution_clock::now();

  SDL_Event event;
  float fps = 60;
  bool keep_going = true;
  bool make_pause = false;
  bool run_once = true;

  while (keep_going)
  {
      // Event Handler
      if (SDL_PollEvent(&event))
      {
          // Close Window Button
          if (event.type == SDL_QUIT) break;

          if (event.type == SDL_WINDOWEVENT) {
            switch (event.window.event) {
              case SDL_WINDOWEVENT_RESIZED:
                renderer.change_resolution(event.window.data1, event.window.data2);
                particles.change_resolution(event.window.data1, event.window.data2);
                break;
              case SDL_WINDOWEVENT_SIZE_CHANGED:
                renderer.change_resolution(event.window.data1, event.window.data2);
                particles.change_resolution(event.window.data1, event.window.data2);
                break;
              // case SDL_WINDOWEVENT_CLOSE:
              //     SDL_Log("Window %d closed", event->window.windowID);
              //     break;
              default:
                break;
            }
        }

          if (event.type == SDL_KEYDOWN){
            switch( event.key.keysym.sym ){
                  case SDLK_p:
                      make_pause = !make_pause;
                      break;
                  case SDLK_r:
                      algorithm.cout_rating();
                      algorithm.reset();
                      std::cout << "----- Reset -----" << std::endl;
                      break;
                  case SDLK_c:
                      particles.change_color(0.75);
                      // Update points
                      for (size_t i = 0; i < population_size; i++) {
                        for (size_t j = 0; j < constellation_size; j++) {
                          particles.m_storage[ i*constellation_size + j ].col   = particles.m_storage[ i*constellation_size].col;
                        }
                      }

                      // Best Particle
                      for (size_t j = 0; j < constellation_size; j++) {
                        particles.m_storage[ best_particle * constellation_size + j ].col   = best_color;
                      }
                      break;
                  case SDLK_SPACE:
                      algorithm.cout_rating();
                      break;
                  case SDLK_UP:
                      fps = fps + 1;
                      break;
                  case SDLK_DOWN:
                      if(fps > 2){
                        fps = fps - 1;
                      }
                      break;
                  case SDLK_RIGHT:
                      run_once = true;
                      make_pause = false;
                      break;
                  case SDLK_ESCAPE:
                      keep_going = false;
                      break;
                  case SDLK_RETURN:
                      renderer.toggle_fullscreen();
                      break;
                  default:
                      break;
              }
          }
      }

      // Graphics Stuff
      tp_end   = std::chrono::high_resolution_clock::now();
      s_time   = std::chrono::duration_cast<std::chrono::duration<double>>(tp_end - tp_start);

      if( s_time.count() > (1.0/fps) && !make_pause){
        if(run_once){
          make_pause = true;
          run_once = false;
        }

        renderer.load_vertex_buffer(particles.get_storage(), particles.get_n_particles());
        renderer.refresh();
        tp_start = std::chrono::high_resolution_clock::now();

        // Algorithm Stuff
        algorithm.update();

        // Update points
        for (size_t i = 0; i < population_size; i++) {
          /* code */
          for (size_t j = 0; j < constellation_size; j++) {
            particles.m_storage[ i*constellation_size + j ].pos.x = algorithm.get_x(i,j);
            particles.m_storage[ i*constellation_size + j ].pos.y = algorithm.get_y(i,j);
          }
        }
        // Best Particle
        for (size_t j = 0; j < constellation_size; j++) {
          particles.m_storage[ best_particle * constellation_size + j ].pos.x = algorithm.get_x_best(j);
          particles.m_storage[ best_particle * constellation_size + j ].pos.y = algorithm.get_y_best(j);
        }

      }
  }

  // Destruction
  //////////////////////////////////////////////////////////////////////////////

  SDL_Quit();
  return 0;
}

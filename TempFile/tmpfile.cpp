#include "tmpfile.h"

FileRx::FileRx(std::string filename):
  m_filename(filename + ".tmp"),
  m_lock(filename + ".lock") {

}

bool FileRx::rx(){
  bool got_file = false;
  // Check if Lock exists
  std::ifstream lock(m_lock);
  if(!lock.good()){
    // lock it
    std::ofstream my_lock(m_lock);
    my_lock.close();
    // openfile
    std::ifstream in(m_filename);
    // Check if file exists
    if(in.good()){
      // Check Header
      int new_id;
      in >> new_id;
      in.ignore(20, ';'); // Header ends with a semicolon

      // Check if the message is a new message!
      if(msg_id != new_id) {
        // Update Message ID
        std::cout << new_id << ": ";
        msg_id = new_id;

        // Read Data
        ////////////////////////////////////////////////////////////////////////
        int a;
        while(in >> a){
          std::cout << a << " ";
        }
        std::cout << std::endl;

        ////////////////////////////////////////////////////////////////////////
        got_file = true;
      }
    }
    // unlock it
    std::remove(m_lock.c_str());
  } else {
    std::cout << "read -> locked" << std::endl;
  }
  std::this_thread::sleep_for(std::chrono::milliseconds(10));
  return got_file;
}



FileTx::FileTx(std::string filename):
  m_filename(filename + ".tmp"),
  m_lock(filename + ".lock") {

  // Unlock if still locked
  std::remove(m_filename.c_str());
  std::remove(m_lock.c_str());
}

bool FileTx::tx(){
  bool got_file = false;
  // Check if Lock exists
  std::ifstream lock(m_lock);
  if(!lock.good()){
    std::cout << "write" << std::endl;

    // lock it
    std::ofstream my_lock(m_lock);
    // my_lock.close();

    // openfile
    std::ofstream out(m_filename);

    // Write Data
    ////////////////////////////////////////////////////////////////////////////
    // Write Simple Header
    msg_id++;
    out << msg_id << ";" << std::endl;

    // Write Data
    std::vector<int> data;
    for (int i = 0; i < 9; i++) {
      data.push_back(i);
    }

    for (int& e: data) {
      out << e << std::endl;
    }
    ////////////////////////////////////////////////////////////////////////////
    out.flush();
    out.close();

    // unlock it
    std::remove(m_lock.c_str());

    got_file = true;
  } else {
    std::cout << "write -> locked" << std::endl;
  }

  std::this_thread::sleep_for(std::chrono::milliseconds(10));
  return got_file;
}

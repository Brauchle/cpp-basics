#ifndef __SIMPLE_TEMPFILE_CLASS__
#define __SIMPLE_TEMPFILE_CLASS__
// Tracks the angle of an object using the kalman filter

#include <iostream>
#include <fstream>
#include <string>

// sleep
#include <thread>
#include <chrono>

// data
#include <vector>

class FileRx {
  private:
    std::string m_filename, m_lock;
    // std::ifstream in;
    int msg_id = 0;

  public:
    FileRx(std::string filename);
    bool rx();
};

class FileTx {
  private:
    std::string m_filename, m_lock;
    // std::ofstream out;
    int msg_id = 0;

  public:
    FileTx(std::string filename);
    bool tx();
};


#endif

#ifndef __SIMPLETIMER__
#define __SIMPLETIMER__

#include <iostream>
#include <chrono>
#include <string>

namespace timer {
  auto start_time = std::chrono::high_resolution_clock::now();

  void tik();
  void tok();
  void tok(std::string label);

  void tik(){
    start_time = std::chrono::high_resolution_clock::now();
  }

  void tok(){
    auto stop_time = std::chrono::high_resolution_clock::now();

    auto ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(stop_time - start_time);
    std::cout << ms_time.count() << std::endl;
  }

  template <typename T> void tok(){
    auto stop_time = std::chrono::high_resolution_clock::now();

    auto ms_time = std::chrono::duration_cast<T>(stop_time - start_time);
    std::cout << ms_time.count() << std::endl;
  }

  void tok(std::string label){
    auto stop_time = std::chrono::high_resolution_clock::now();

    auto ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(stop_time - start_time);
    std::cout << label << ms_time.count() << std::endl;
  }

  template <typename T> void tok(std::string label){
    auto stop_time = std::chrono::high_resolution_clock::now();

    auto ms_time = std::chrono::duration_cast<T>(stop_time - start_time);
    std::cout << label << ms_time.count() << std::endl;
  }
}

#endif

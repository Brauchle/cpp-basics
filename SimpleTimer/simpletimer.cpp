#include "simpletimer.h"

namespace timer {
  void timer::tik(){
    start_time = std::chrono::high_resolution_clock::now();
  }

  void tok(){
    auto t2 = std::chrono::high_resolution_clock::now();

    auto ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-start_time);
    std::cout << ms_time.count() << std::endl;
  }

  void tok(std::string label){
    auto t2 = std::chrono::high_resolution_clock::now();

    auto ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-start_time);
    std::cout << label << ": " ms_time.count() << std::endl;
  }
}

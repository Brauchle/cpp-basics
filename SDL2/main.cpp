#include <iostream>
#include "sdlwrapper.h"


int main(int, char**){
	sdl_init();

  SDL_SetRenderDrawColor(SDL::REN, 0, 0, 0, SDL_ALPHA_OPAQUE);
  SDL_RenderClear(SDL::REN);

  SDL_SetRenderDrawColor(SDL::REN, 255, 255, 255, SDL_ALPHA_OPAQUE);
  SDL_RenderDrawLine(SDL::REN, 320, 200, 300, 240);
  SDL_RenderDrawLine(SDL::REN, 300, 240, 340, 240);
  SDL_RenderDrawLine(SDL::REN, 340, 240, 320, 200);
  SDL_RenderDrawPoint(SDL::REN, 10, 10);
  SDL_RenderPresent(SDL::REN);

  SDL_Delay(4000);

	sdl_exit();
	return 0;
}

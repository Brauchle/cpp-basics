#ifndef __SDL__WRAPPER__
#define __SDL__WRAPPER__

#include <SDL2/SDL.h>

namespace SDL {
}

class SDLWrapper {
  public:
    SDLWrapper(int win_width, int win_height);
    ~SDLWrapper();

    SDL_Renderer *ren(){ return m_renderer; }
    int w(){return m_win_width; }
    int h(){return m_win_height; };
  private:
    int m_win_width;
    int m_win_height;

    SDL_Window   *m_window;
    SDL_Renderer *m_renderer;
};

#endif

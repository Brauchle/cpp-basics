#ifndef __GEN_ALG_FUNCTIONS__
#define __GEN_ALG_FUNCTIONS__

#include <vector>
#include <complex>
#include <random>
#include <algorithm>

#include <iostream>
#include <fstream>
#include <string>

using cfloat   = std::complex<float>;
using v_cfloat = std::vector<cfloat>;

struct Individuum{
  v_cfloat genes;
  float fitness;
};

using Population = std::vector<Individuum>;

float rate(Individuum& a);

void init(Population& pop, int genes);

void selection(Population& pop);

void reproduce(Population& pop, int n);

void mutate(Population& pop, int n, float chance);

void store_generation(std::ofstream& out, Population& pop);

#endif

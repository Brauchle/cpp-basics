#include "sdlwrapper.h"


SDLWrapper::SDLWrapper(int win_width, int win_height):
  m_win_width(win_width),
  m_win_height(win_height)
{
  SDL_Init(SDL_INIT_VIDEO);

  m_window = SDL_CreateWindow("Hello World!", 100, 100, m_win_width,
                              m_win_height, SDL_WINDOW_SHOWN);
  if (m_window == nullptr) {
    SDL_Quit();
  }

  m_renderer = SDL_CreateRenderer(
      m_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (m_renderer == nullptr) {
    SDL_DestroyWindow(m_window);
    SDL_Quit();
  }
}

SDLWrapper::~SDLWrapper() {
  SDL_DestroyRenderer(m_renderer);
  SDL_DestroyWindow(m_window);

  SDL_Quit();
}

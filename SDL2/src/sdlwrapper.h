#ifndef __SDL__WRAPPER__
#define __SDL__WRAPPER__

#include <SDL2/SDL.h>

namespace SDL {
  const int SCREEN_WIDTH  = 640;
  const int SCREEN_HEIGHT = 480;

  extern SDL_Window   *WIN;
  extern SDL_Renderer *REN;
}

bool sdl_init();
bool sdl_exit();

#endif

cmake_minimum_required(VERSION 2.6)
project(SDLExample)
# switch to clang : export CXX=/usr/bin/clang++
# switch to c++   : export CXX=/usr/bin/g++

# Welcome Message
################################################################################
message("Trying to take over your system - done.")
# For Clang-Tidy
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# Build Type (Debug -g, Release -O3 -DNDEBUG)
################################################################################
set(CMAKE_BUILD_TYPE Debug)

# Dependendcy Check
################################################################################
find_package(PkgConfig)

# SDL2
pkg_check_modules(SDL sdl2)

link_directories(
    ${SDL_LIBRARY_DIRS}
    ${SDL_LIBRARY_DIRS}
)

# File Collection
################################################################################
file(GLOB_RECURSE sources      src/*.cpp src/*.h)

# Add Subfolder
################################################################################
include_directories(
    ${CMAKE_SOURCE_DIR}/src
)

# Compiler and Linker Flags
################################################################################
set(CMAKE_CXX_FLAGS "-std=c++17 -Wall -Ofast -mavx -mfma -march=native")

message("CMAKE_CXX_FLAGS_DEBUG is ${CMAKE_CXX_FLAGS_DEBUG}")
message("CMAKE_CXX_FLAGS_RELEASE is ${CMAKE_CXX_FLAGS_RELEASE}")

# Targets
################################################################################
# Targets
add_executable(main main.cpp ${sources})
target_link_libraries(main
    ${SDL_LIBRARIES}
  )


# Clang-Tidy
################################################################################
file(GLOB_RECURSE ALL_SOURCE_FILES src/*.cpp)

add_custom_target(
        clang-format
        COMMAND /usr/bin/clang-format
        -style=Google
        -i
        ${ALL_SOURCE_FILES}
)

add_custom_target(
        clang-tidy
        COMMAND /usr/bin/clang-tidy
        -p ${CMAKE_SOURCE_DIR}/build
        ${ALL_SOURCE_FILES}
)

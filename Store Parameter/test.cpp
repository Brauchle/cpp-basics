#include "parameters.h"

#include <iostream>

int main(int argc, char const *argv[]) {
  Parameter test;
  test.param_a = 1;
  test.param_b = 0;
  test.param_c = 2;

  test = parse_parameter(argc, argv);

  store_parameter(test, "test_file.init");

  Parameter test_in;
  load_parameter(&test_in, "test_file.init");

  std::cout << test.param_a << std::endl;
  std::cout << test.param_b << std::endl;
  std::cout << test.param_c << std::endl;

  return 0;
}

#ifndef SRC_FILEOPS_LOADFILE_H
#define SRC_FILEOPS_LOADFILE_H

#include <string>
#include <iostream>
#include <fstream>

struct Parameter {
  float param_a;
  float param_b;
  float param_c;
  float param_d;
  float param_e;
  float param_f;
};

void load_parameter( Parameter* param, const std::string& p_filename ){
  std::ifstream in(p_filename, std::ios::binary);
  in.read((char*)param, sizeof(Parameter));
}

void store_parameter( Parameter param, const std::string& p_filename ){
  std::ofstream out(p_filename, std::ios::binary);
  out.write((char*)&param, sizeof(Parameter));
}

Parameter parse_parameter(int argc, char const *argv[]) {
// Parse Input
////////////////////////////////////////////////////////////////////////////////
  Parameter param;

  size_t cnt = 0;

  if(argc == 4){
    param.param_a = std::atoi(argv[1]);
    param.param_b = std::atoi(argv[2]);
    param.param_c = std::atoi(argv[3]);
  } else {
    std::cout << "No or wrong Number of Arguments, using default values" << std::endl;
  }

  return param;
}

#endif
